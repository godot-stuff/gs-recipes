
#
#	Godot has the ability to provide Script Templates
#	when you are developing.
#
#	Official documentation is here
#
#		https://docs.godotengine.org/en/stable/getting_started/scripting/creating_script_templates.html?highlight=template
#
#	You can add templates to your project in this special folder
#
#		res://script_templates
#
#	The path above can always be changed to be something different by modifying the Project setting
#
#		editor/script_templates_search_path
#
#	You can also add templates to your computer in a special folder called
#
#		Windows:	%appdata%\godot\script_templates
#		Linux:		$HOME/.local/share/godot/script_templates/
#		Mac:		$HOME/Library/Application Support/Godot/script_templates/
#
