
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name TickMove
extends Component

export var tps : int = 30
export var speed : float = 2.0
