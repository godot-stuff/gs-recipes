extends Node2D

export var width : int = 15
export var height : int = 8
export var offset : Vector2 = Vector2(1, 0.75)

onready var tile = preload("res://ecs-ticks/tile.tscn")

func _ready():
	
	for w in width:
		
		for h in height:
			
			var _i = tile.instance()
			add_child(_i)
			_i.global_position = (Vector2(w, h) * 64.0) + (offset * 64.0)
	
