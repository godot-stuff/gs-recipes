class_name MoveActionSystem
extends System

#
#	this action will randomly pick a direction
#	for the entity to move and then sets the
#	location based on that direction
#
func on_process_entity(entity :  Entity, delta : float):
	
	var _action_tick = entity.get_component("actiontick") as ActionTick
	var _move = entity.get_component("move") as Move
	var _grid_move = entity.get_component("gridmove") as GridMove
	
	if (_action_tick.has_action):
		
		# reset the action flag
		_action_tick.has_action = false
		
		# if we are already moving, exit
		if (_move.is_moving):
			return
		
		# get a new grid location
		_grid_move.next_grid = _grid_move.curr_grid + _get_random_direction()
		
		# if we are beyond the x edge hold it there
		if (_grid_move.next_grid.x < 0 or _grid_move.next_grid.x > 9):
			_grid_move.next_grid.x = _grid_move.curr_grid.x
		
		# if we are beyond the y edge hold it there
		if (_grid_move.next_grid.y < 0 or _grid_move.next_grid.y > 9):
			_grid_move.next_grid.y = _grid_move.curr_grid.y
			
#		print(_grid_move.next_grid)
		
		
func _get_random_direction():
	
	var _r : int = int(rand_range(0, 4)) + 1
	
#	print("- new location %s" % [_r])
	
	if (_r == 1):
		return Vector2.UP
		
	if (_r == 2):
		return Vector2.DOWN
		
	if (_r == 3):
		return Vector2.LEFT
		
	if (_r == 4):
		return Vector2.RIGHT
		
	return Vector2.ZERO
	
