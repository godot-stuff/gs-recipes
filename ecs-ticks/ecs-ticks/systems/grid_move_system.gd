class_name GridMoveSystem
extends System

var offset : Vector2 = Vector2(1, 0.75) * 64

func on_process_entity(entity : Entity, delta : float):
	
	var _grid_move = entity.get_component("gridmove") as GridMove
	
	if (_grid_move.next_grid != _grid_move.curr_grid):
		_grid_move.curr_grid = _grid_move.next_grid
		entity.global_position = (_grid_move.curr_grid * 64) + Vector2(32, 32)
