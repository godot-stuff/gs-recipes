
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name TickMoveSystem
extends System

#
func on_process_entity(entity :  Entity, delta : float):
	
	var _tickmove = entity.get_component('tickmove') as TickMove 
	var _move = entity.get_component('move') as Move
	var _tick = entity.get_component('tick') as Tick
	
	# tick speed factor
	var _sf : float =  _tick.tps / _tickmove.tps
	# time to move one pixel
	var _ps : float = 64.0 / 60.0
	# adjusted pixel speed
	var _as : float = _ps * _sf
	# adjusted speed
	var _fs : float = _as * (1.0 / _tickmove.speed)
	
	
	entity.position += _move.direction * _fs

