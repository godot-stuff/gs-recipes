class_name TickActionSystem
extends System

# will increase the action tick count on a each tick


func on_process_entity(entity :  Entity, delta : float):
	
	var _tick = entity.get_component("tick") as Tick
	var _action_tick = entity.get_component("actiontick") as ActionTick
	
#	
	if (_tick.tick != _action_tick.last_tick):
		_action_tick.last_tick = _tick.tick
		_action_tick.tick += 1
		Logger.fine("- action tick %s" % [_action_tick.tick])

	if (_action_tick.tick >= _action_tick.ticks_per_action):
		_action_tick.tick = 0
		_action_tick.actions += 1
		_action_tick.has_action = true
		Logger.fine("action %s" % [_action_tick.actions])
	
