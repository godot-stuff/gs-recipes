class_name TickSystem
extends System

export var ticks_per_second : float = 60.0

#
# increase the tick of every entity
#
# every entity in the game runs on a base number
# of ticks that are processed here
#
# actions are triggered when their tick count
# reaches a certain number. it can also control
# other non entity things like reloading etc
#

func on_process_entity(entity :  Entity, delta : float):
	
	var _tick = entity.get_component("tick") as Tick
	
	_tick.tps = ticks_per_second
	_tick.tick_count += ticks_per_second * delta
	
	if (_tick.tick_count >= 1.0):
		_tick.tick += 1
		_tick.tick_count = _tick.tick_count - 1.0
		
	
	Logger.fine("- tick %s" % [_tick.tick])
	
