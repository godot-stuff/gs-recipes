
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MoveToSystem
extends System


func on_process_entity(entity : Entity, delta : float) -> void:
	
	var _gridmove  = entity.get_component("gridmove") as GridMove
	var _move = entity.get_component("move") as Move
	var _moveto = entity.get_component("moveto") as MoveToComponent
	
	if (_gridmove.curr_grid != _gridmove.next_grid):
		_move.is_moving = true
		_moveto.next_position = _gridmove.next_grid * 64 + Vector2(32, 32)
	
		var _d = entity.global_position.distance_to(_moveto.next_position)
#		var _d = entity.global_position - _moveto.next_position
		var _dir = entity.global_position.direction_to(_moveto.next_position).normalized()
		Logger.fine(str(_dir))
			
		if (_d > 5.0):
			_move.direction = entity.global_position.direction_to(_moveto.next_position).normalized()
			_move.is_moving = true
		else:
			_move.is_moving = false
			_move.direction = Vector2.ZERO
			_gridmove.last_grid = _gridmove.curr_grid
			_gridmove.curr_grid = _gridmove.next_grid
			entity.global_position = _moveto.next_position
			_moveto.next_position = Vector2.ZERO
			
		
	
	Logger.fine("is_moving: %s; curr_grid:%s; next_grid: %s" % [_move.is_moving, _gridmove.curr_grid, _gridmove.next_grid])
	Logger.fine("global_pos: %s; next_pos: %s" % [entity.global_position, _moveto.next_position])
