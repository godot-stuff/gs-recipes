class_name MoveSystem
extends System

func _ready():
	pass
	
func on_process_entity(entity : Entity, delta : float) -> void:
	
	var _move = entity.get_component("move") as Move
	entity.position += _move.direction * _move.speed * delta
	
	
