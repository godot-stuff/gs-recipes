extends Node2D

export var red_count : int = 10
export var green_count : int = 10
export var blue_count : int = 10

onready var entity_blue : PackedScene = preload("res://ecs-ticks/entities/entity_blue.tscn")
onready var entity_green : PackedScene = preload("res://ecs-ticks/entities/entity_green.tscn")
onready var entity_red : PackedScene = preload("res://ecs-ticks/entities/entity_red.tscn")

var offset : Vector2 = Vector2(1, 0.75) * 64

var state = 0
var next_state = 0

func _ready():
	pass
	

func _process(delta):
	
	match state:
		
		0:
			for i in red_count:
				
				var _e = entity_red.instance()
				add_child(_e)
				
			for i in green_count:
				
				var _e = entity_green.instance()
				add_child(_e)
				
			for i in blue_count:
				
				var _e = entity_blue.instance()
				add_child(_e)

			next_state = 1

		1:
			Logger.fine(str(ECS.entities))
			
			for e in ECS.entities:
				
				var _e = ECS.entities[e] as Entity
				var _grid_move = _e.get_component("gridmove") as GridMove
				_grid_move.next_grid = Vector2(7, 3)
				_grid_move.curr_grid = Vector2(7, 3)
				_e.global_position = (_grid_move.next_grid * 64) + Vector2(32, 32)
			
			next_state = 2
			
		2:
			ECS.update()
			

	state = next_state

	$Node/FpsLabel.text = "FPS : %s" % [Engine.get_frames_per_second()]
	$Node/EntitiesLabel.text = "Entities : %s" % [ECS.entities.size()]
	


func _on_TicksValue_value_changed(value):
	$Node/TicksLabel.text = "Ticks Per Second : %s" % [value]
	$SystemTick.ticks_per_second = value
