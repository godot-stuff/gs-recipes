extends Node2D

export var entities : int = 10

onready var entity : PackedScene = preload("res://ecs-ticks/entities/entity_blue.tscn")

func _ready():
	
	for i in entities:
		
		var _e = entity.instance()
		add_child(_e)
		var _grid_move = _e.get_component("gridmove") as GridMove
		_grid_move.last_grid = Vector2(0,0)
		_grid_move.curr_grid = Vector2(4,5)
		_grid_move.next_grid = Vector2(4,5)
		_e.global_position = (_grid_move.curr_grid * 64) + Vector2(32,32)
		
	ECS.rebuild()
	
func _process(delta):
	ECS.update()


func _on_Timer_timeout():
	Core.quit()
