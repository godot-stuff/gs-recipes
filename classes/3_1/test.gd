extends Node
class_name TestAnimals

const Lion = preload("./lion.gd")
const Animal = preload("./animal.gd")


func _speak(animal : Animal):
	if animal is Animal:
		print("the %d year old '%s', says '%s'" % [animal.age, animal.name, animal.speak() ])

func _ready():

	var animal = Animal.new()
	var lion = Lion.new()

	_speak(animal)
	_speak(lion)


func _init():
	print("Hello World")