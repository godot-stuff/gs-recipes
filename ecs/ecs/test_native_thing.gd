extends Node2D

export (int) var ANGLE
export (bool) var RANDOM_ANGLE
export (int) var SPEED
export (int) var SPEED_FACTOR

export (bool) var USE_VIEWPORT
export (Vector2) var TOP_LEFT
export (Vector2) var BOTTOM_RIGHT

var dir


func _process(delta):
	var _vel = dir * SPEED * SPEED_FACTOR
	
	position += _vel * delta

	if position.x < TOP_LEFT.x:
		position.x = BOTTOM_RIGHT.x
		
	if position.x > BOTTOM_RIGHT.x:
		position.x = TOP_LEFT.x

	if position.y < TOP_LEFT.y:
		position.y = BOTTOM_RIGHT.y
			
	if position.y > BOTTOM_RIGHT.y:
		position.y = TOP_LEFT.y
			

func _ready():
	
	if RANDOM_ANGLE:
		ANGLE = floor(rand_range(0, 360))
		
	dir = Vector2(cos(deg2rad(ANGLE-90)), sin(deg2rad(ANGLE-90)))

	if USE_VIEWPORT:
		TOP_LEFT = Vector2(0,0)
		BOTTOM_RIGHT = get_viewport().get_visible_rect().size
		
#	set_physics_process(false)
	