extends Node

export (int) var COMPONENTS = 10
export (int) var BURST = 20

#onready var Thing = preload("res://ecs/test_native_thing.tscn")
#onready var Thing = preload("res://ecs/test_thing.tscn")
#onready var Thing = preload("res://ecs/components/component.tscn")
#onready var Thing = preload("res://ecs/components/area2d_component.tscn")
#onready var Thing = preload("res://ecs/components/node2d_component.tscn")
onready var Thing = preload("res://ecs/components/kinematic2d_component.tscn")

var count = 0

func _process(delta):
	$CanvasLayer/Label.text = "count: %s fps: %s" % [str(count), str(Engine.get_frames_per_second())]

func _on_Timer_timeout():
	
	$Timer.stop()
	
	if count >= COMPONENTS:
		return
		get_tree().quit()
		
	for i in range(BURST):
		var t = Thing.instance()
		t.position = Vector2(480, 240)
		add_child(t)
		count += 1
	
	$Timer.start()