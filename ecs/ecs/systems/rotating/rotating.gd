extends Node

export (int) var rotation_direction = 1
export (int) var rotation_speed = 5

onready var Component = preload("res://ecs/components/component.gd")


func _on_pre_move(component):
	component.rotation += rotation_direction * rotation_speed * get_process_delta_time()


func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_PRE_MOVE, self, "_on_pre_move")

