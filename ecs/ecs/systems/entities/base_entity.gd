
extends Node


const COMPONENT_PRE_MOVE = "pre_move"
const COMPONENT_MOVE = "move"
const COMPONENT_POST_MOVE = "post_move"
const COMPONENT_INPUT = "input"
const COMPONENT_COLLISION = "collision"


signal pre_move(this)
signal move(this)
signal post_move(this)
signal input(this)
