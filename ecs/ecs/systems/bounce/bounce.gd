extends Node

export (bool) var USE_VIEWPORT
export (Vector2) var TOP_LEFT
export (Vector2) var BOTTOM_RIGHT


onready var Component = preload("res://ecs/components/component.gd")


func _post_move(component):
#	Logger.trace("move_clamp._post_move")
	
	if component.position.x < TOP_LEFT.x:
		component.direction.x = component.direction.x * -1
		
	if component.position.x > BOTTOM_RIGHT.x:
		component.direction.x = component.direction.x * -1

	if component.position.y < TOP_LEFT.y:
		component.direction.y = component.direction.y * -1
			
	if component.position.y > BOTTOM_RIGHT.y:
		component.direction.y = component.direction.y * -1
			
func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_POST_MOVE, self, "_post_move")
		
	if USE_VIEWPORT:
		TOP_LEFT = Vector2(0,0)
		BOTTOM_RIGHT = get_viewport().get_visible_rect().size
		