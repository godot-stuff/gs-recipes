extends Node

export (int) var AMOUNT

const COMPONENT_PRE_MOVE = "pre_move"
const COMPONENT_MOVE = "move"
const COMPONENT_POST_MOVE = "post_move"
const COMPONENT_INPUT = "input"
const COMPONENT_COLLISION = "collision"


signal pre_move(this)
signal move(this)
signal post_move(this)
signal input(this)

var last_velocity

func _on_post_move(component):
	
	pass
	
	
func _ready():
	
	if owner:
		get_parent().connect(COMPONENT_POST_MOVE, self, "_on_post_move")
