extends Node

export (int) var rotation_speed = 5

onready var Component = preload("res://ecs/components/component.gd")

var _rotation
var _velocity
var _speed
var _speed_factor

func _on_input(component):
	
	_rotation = 0
	_velocity = Vector2()
	
	if Input.is_action_pressed("right"):
		_rotation += 1
		return

	if Input.is_action_pressed("left"):
		_rotation -= 1
		return
		
	if Input.is_action_pressed("up"):
		_velocity = Vector2(-1 * _speed * _speed_factor, 0).rotated(component.rotation)
		return
		
	if Input.is_action_pressed("down"):
		_velocity = Vector2(1 * _speed * _speed_factor, 0).rotated(component.rotation)
		return
		
	
func _on_pre_move(component):
	component.rotation += _rotation * rotation_speed * get_process_delta_time()
	component.velocity += _velocity
		
func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_INPUT, self, "_on_input")
		get_parent().connect(Component.COMPONENT_PRE_MOVE, self, "_on_pre_move")
		_speed = get_parent().speed
		_speed_factor = get_parent().speed_factor