extends Node

onready var Component = preload("res://ecs/components/component.gd")

func _on_input(component):
	
	component.direction = Vector2()
	
	if Input.is_action_pressed("right"):
		component.direction = Vector2(1, 0)
		return

	if Input.is_action_pressed("left"):
		component.direction = Vector2(-1, 0)
		return
		
	if Input.is_action_pressed("up"):
		component.direction = Vector2(0, -1)
		return
		
	if Input.is_action_pressed("down"):
		component.direction = Vector2(0, 1)
		return
		
func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_INPUT, self, "_on_input")
