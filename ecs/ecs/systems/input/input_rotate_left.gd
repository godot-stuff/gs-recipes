extends Node


onready var Component = preload("res://ecs/components/component.gd")


export (String) var ACTION = "NONE"

const COMPONENT_PRE_MOVE = "pre_move"
const COMPONENT_MOVE = "move"
const COMPONENT_POST_MOVE = "post_move"
const COMPONENT_INPUT = "input"
const COMPONENT_COLLISION = "collision"

signal pre_move(this)
signal move(this)
signal post_move(this)
signal input(this)

var action


func _on_pre_move(component):
	if action:
		emit_signal(COMPONENT_PRE_MOVE, component)


func _on_input(component):
	
	action = false
	
	if Input.is_action_pressed(ACTION):
		action = true

func _ready():
	
	if owner:
		get_parent().connect(COMPONENT_INPUT, self, "_on_input")
		get_parent().connect(COMPONENT_PRE_MOVE, self, "_on_pre_move")