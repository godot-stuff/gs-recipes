extends Node


onready var Component = preload("res://ecs/components/component.gd")


func _on_collision(component, collider):
	print("before", component.direction)
	component.direction = collider.remainder.bounce(collider.normal).normalized()
	print("after", component.direction)
	
	
func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_COLLISION, self, "_on_collision")
		
	
