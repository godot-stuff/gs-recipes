extends Area2D

export (int) var SPEED
export (int) var SPEED_VARIANCE
export (Vector2) var DIRECTION


onready var Component = preload("res://ecs/components/component.gd")


func _pre_move(component):
	Logger.trace("wind._pre_move")
	var _var = rand_range(-SPEED_VARIANCE, SPEED_VARIANCE)
	var _vel = DIRECTION * (SPEED + _var)
	Logger.trace("wind velocity:%s" % [_vel])
	component.velocity += _vel
	
	
func _ready():
	
	if not DIRECTION:
		DIRECTION = Vector2(0, 0)
				

func _on_Wind_area_entered(area):
	area.connect(Component.COMPONENT_PRE_MOVE, self, "_pre_move")
	


func _on_Wind_area_exited(area):
	area.disconnect(Component.COMPONENT_PRE_MOVE, self, "_pre_move")
