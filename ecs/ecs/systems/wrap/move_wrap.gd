extends Node

export (bool) var USE_VIEWPORT
export (Vector2) var TOP_LEFT
export (Vector2) var BOTTOM_RIGHT


onready var Component = preload("res://ecs/components/component.gd")


func _post_move(component):
#	Logger.trace("move_clamp._post_move")
	
	if component.position.x < TOP_LEFT.x:
		component.position.x = BOTTOM_RIGHT.x
		
	if component.position.x > BOTTOM_RIGHT.x:
		component.position.x = TOP_LEFT.x

	if component.position.y < TOP_LEFT.y:
		component.position.y = BOTTOM_RIGHT.y
			
	if component.position.y > BOTTOM_RIGHT.y:
		component.position.y = TOP_LEFT.y
			
func _ready():
	
	if owner:
		get_parent().connect(Component.COMPONENT_POST_MOVE, self, "_post_move")
		
	if USE_VIEWPORT:
		TOP_LEFT = Vector2(0,0)
		BOTTOM_RIGHT = get_viewport().get_visible_rect().size
		