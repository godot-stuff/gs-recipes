extends "../entities/base_entity.gd"

export (int) var THRUST = 1
export (int) var THRUST_FACTOR = 10


func _on_pre_move(component):
	component.velocity += Vector2(-1 * THRUST * THRUST_FACTOR, 0).rotated(component.rotation)

func _ready():
	
	if owner:
		get_parent().connect(COMPONENT_PRE_MOVE, self, "_on_pre_move")
