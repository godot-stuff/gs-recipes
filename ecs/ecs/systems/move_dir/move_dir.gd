extends Node

export (int) var ANGLE
export (bool) var RANDOM_ANGLE
export (int) var SPEED
export (int) var SPEED_FACTOR

onready var Component = preload("res://ecs/components/component.gd")

func _ready():
	
	if owner:
		
		if RANDOM_ANGLE:
			ANGLE = floor(rand_range(0, 360))
			
		var _dir = Vector2(cos(deg2rad(ANGLE-90)), sin(deg2rad(ANGLE-90)))
		
		get_parent().direction = _dir
