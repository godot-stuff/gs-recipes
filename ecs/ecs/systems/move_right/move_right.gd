extends Node

export (int) var SPEED
export (int) var SPEED_FACTOR

onready var Component = preload("res://ecs/components/component.gd")

func _ready():
	
	if owner is Component:
		
		var _dir = Vector2(1, 0)
		
		get_parent().direction = _dir
		
		if SPEED:
			get_parent().speed = SPEED
		
		if SPEED_FACTOR:
			get_parent().speed_factor = SPEED_FACTOR