extends KinematicBody2D


export (int) var speed = 5
export (int) var speed_factor = 10
export (Vector2) var direction = Vector2(0, 0)
export (int) var angle = 0

var velocity = 0

const COMPONENT_PRE_MOVE = "before_move"
const COMPONENT_PRE_MOVE = "pre_move"
const COMPONENT_MOVE = "move"
const COMPONENT_POST_MOVE = "post_move"
const COMPONENT_POST_MOVE = "after_move"
const COMPONENT_INPUT = "input"
const COMPONENT_COLLISION = "collision"


signal pre_move(this)
signal move(this)
signal post_move(this)
signal input(this)
signal collision(this, collider)
signal before_move(this)
signal after_move(this)


func _process(delta):
	
	velocity = Vector2(0, 0)
	
	emit_signal(COMPONENT_INPUT, self)
	
	emit_signal(COMPONENT_PRE_MOVE, self)

	emit_signal(COMPONENT_MOVE, self)

	velocity += direction * speed * speed_factor
#	position += velocity * delta
	var collider = move_and_collide(velocity * delta)

	if collider:
		emit_signal(COMPONENT_COLLISION, self, collider)
		
	emit_signal(COMPONENT_POST_MOVE, self)
	
func _ready():
	Logger.set_logger_level(Logger.LEVEL_WARN)
	velocity = Vector2(0, 0)
#	set_physics_process(false)

