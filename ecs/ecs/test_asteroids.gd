extends Node2D

onready var rock = preload("res://ecs/asteroid.tscn")

func _process(delta):
	
	if Input.is_action_just_pressed("button"):
		var r = rock.instance()
		add_child(r)
		r.position = get_global_mouse_position()
		
func _ready():
	pass
