shader_type canvas_item;
render_mode blend_mix;

void fragment() {
	vec4 color = texture(SCREEN_TEXTURE, SCREEN_UV);
	float gray_value = (color.r + color.g + color.b) / 3.0;
	COLOR.rgb = vec3(gray_value);
}