extends Node

export (int) var count_max = 10
export (PackedScene) var test_scene
export (bool) var use_ecs = false

var count = 0

func _process(delta):
	if use_ecs:
		gecs.update("Systems", delta)

	$Debug/Label.text = "fps: %s\nentities: %s" % [Engine.get_frames_per_second(), gecs._entities.size()]


func _add_entity():
	if count > count_max:
		return

	var t = test_scene.instance()
	t.position = Vector2(270, 480)
	add_child(t)
	count += 1



func _init():
	gecs.clear()


func _on_Timer_timeout():
	_add_entity()
