extends "../test_system.gd"

"""
	checks if the player has died :(

"""

signal player_dead

func process(entities, delta):
	for entity in entities:
		if entity.components["player"].dead:
			entity.position = Vector2(10000, 10000)
			entity.enabled = false
			TestGame.emit_signal("player_dead")

#func on_ready():
#	gecs.add_system(self, ["player"])

