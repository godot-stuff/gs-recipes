extends "../test_system.gd"

"""
	checks for the player hitting anything,
	usually resulting in death :)
"""

func process(entities, delta):
	for entity in entities:
		var player = entity.components['player']
		var o = entity.get_overlapping_areas()
		if o.size() > 0:
			var a = o[0]
			print("player collided with %s" % [a.name])
			player.dead = true

#func on_ready():
#	gecs.add_system(self, ["player"])

