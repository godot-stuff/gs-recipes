extends "../test_system.gd"

const laser = preload("res://gecs_test/entities/test_laser.tscn")


func process(entities, delta):
	for entity in entities:
		if Input.is_action_just_pressed("fire"):
			var l = laser.instance()
			l.rotation = entity.rotation
			l.position = entity.position + Vector2(55, 0).rotated(entity.rotation)
			add_child(l)

#func on_ready():
#	gecs.add_system(self, ["player"])

