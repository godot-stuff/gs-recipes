extends "../test_system.gd"


func process(entities, delta):
	for entity in entities:

		var player = entity.components['player']
		var velocity = entity.components['velocity']
		var rotatable = entity.components['rotatable']

		var _rotation = 0
		var _velocity = Vector2()

		if Input.is_action_pressed("right"):
			_rotation += 1

		if Input.is_action_pressed("left"):
			_rotation -= 1

		if Input.is_action_pressed("up"):
			_velocity = Vector2(1 * velocity.speed * velocity.speed_factor, 0).rotated(entity.rotation)

		rotatable.rotation += _rotation
		velocity.velocity += _velocity

#func on_ready():
#	gecs.add_system(self, ["player"])

