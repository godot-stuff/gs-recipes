extends "../test_system.gd"


func process(entities, delta):
	for entity in entities:

		var velocity = entity.components['velocity']
		var rotatable = entity.components['rotatable']

		entity.rotation += rotatable.rotation * rotatable.speed * delta
		entity.position += velocity.velocity * delta

		#	reset rotation and velocity

		velocity.velocity = Vector2()
		rotatable.rotation = 0

#func on_ready():
#	gecs.add_system(self, ["player"])

