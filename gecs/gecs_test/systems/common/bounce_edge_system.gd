extends "../test_system.gd"

export (bool) var USE_VIEWPORT = true
export (Rect2) var BOUNDRY = Rect2()
export (Rect2) var MARGIN = Rect2()


func process(entities, delta):
	for entity in entities:

		if entity.position.x > BOUNDRY.end.x:
			entity.position.x = BOUNDRY.end.x
			entity.components['velocity'].direction.x = entity.components['velocity'].direction.x * -1


		if entity.position.x < BOUNDRY.position.x:
			entity.position.x = BOUNDRY.position.x
			entity.components['velocity'].direction.x = entity.components['velocity'].direction.x * -1


		if entity.position.y > BOUNDRY.end.y:
			entity.position.y = BOUNDRY.end.y
			entity.components['velocity'].direction.y = entity.components['velocity'].direction.y * -1


		if entity.position.y < BOUNDRY.position.y:
			entity.position.y = BOUNDRY.position.y
			entity.components['velocity'].direction.y = entity.components['velocity'].direction.y * -1



func on_ready():

#	gecs.add_system(self, ["position", "velocity"])

	if USE_VIEWPORT:
		BOUNDRY = Rect2(0, 0, get_viewport().get_visible_rect().size.x, get_viewport().get_visible_rect().size.y)
		MARGIN = Rect2(5, 5, 5, 5)
