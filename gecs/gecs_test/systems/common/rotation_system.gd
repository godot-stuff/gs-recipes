extends "../test_system.gd"


func process(entities, delta):
	for entity in entities:
		var rot = entity.components['rotating']
		rot.rotation += rot.direction * rot.speed * rot.speed_factor * delta
		entity.rotation = rot.rotation

#func on_ready():
#	gecs.add_system(self, ["rotating"])

