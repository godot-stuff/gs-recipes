
extends "../test_system.gd"

"""

	checks for the laser hitting anything,
	usually resulting in death :)

"""

func process(entities, delta):

	for entity in entities:

		var asteroid = entity.components["asteroid"]

		if asteroid.destroyed:

			if asteroid.can_break:

				for i in range(asteroid.break_count):
					var a = asteroid.break_asteroid.instance()
					a.position = entity.position
					entity.get_parent().add_child(a)

			entity.position = Vector2(10000, 10000)
			entity.enabled = false
			gecs.remove_entity(entity)

			TestGame.emit_signal("asteroid_destroyed")


#func on_ready():
#	gecs.add_system(self, ["asteroid"])

