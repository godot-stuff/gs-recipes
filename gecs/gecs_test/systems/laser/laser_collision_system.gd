extends "../test_system.gd"

"""

	checks for the laser hitting anything,
	usually resulting in death :)

"""

func process(entities, delta):
	for entity in entities:
		var o = entity.get_overlapping_areas()
		if o.size() > 0:
			var a = o[0]
			print("laser collided with %s" % [a.name])
			if a.name.find("TestAsteroid") >=0:
				a.components["asteroid"].destroyed = true
				entity.position = Vector2(10000, 10000)
				entity.enabled = false
				gecs.remove_entity(entity)
				TestGame.emit_signal("add_score", 5)

#func on_ready():
#	gecs.add_system(self, ["laser"])

