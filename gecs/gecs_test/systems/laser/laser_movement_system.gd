extends "../test_system.gd"


func process(entities, delta):
	for entity in entities:
		var pos = entity.components['position']
		var vel = entity.components['velocity']
		vel.velocity += Vector2(1 * vel.speed * vel.speed_factor, 0).rotated(entity.rotation)
		entity.position += vel.velocity * delta

#		var collision = entity.move_and_collide(vel.velocity * delta)
#
#		entity.components['collidable'].collision = null
#		entity.components['collidable'].has_collided = false
#		entity.components['collidable'].entity = null
#
#		if collision:
#			entity.components['collidable'].collision = collision
#			entity.components['collidable'].has_collided = true
#			entity.components['collidable'].entity = collision.collider
#
#		entity.components['position'].position =  entity.position

		#	reset the velocity
		vel.velocity = Vector2()


#func on_ready():
#	gecs.add_system(self, ["laser"])

