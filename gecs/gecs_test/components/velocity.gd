extends Node

export (int) var SPEED = 5
export (int) var SPEED_FACTOR = 10

var speed = 5
var speed_factor = 10
var direction = Vector2()
var velocity = Vector2()

func _ready():

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR