extends Node

export (bool) var CAN_BREAK
export (PackedScene) var BREAK_ASTEROID
export (int) var BREAK_COUNT

var destroyed = false
var can_break = false
var break_asteroid = null
var break_count = 0


func _ready():

	if CAN_BREAK: can_break = CAN_BREAK
	if BREAK_ASTEROID: break_asteroid = BREAK_ASTEROID
	if BREAK_COUNT: break_count = BREAK_COUNT
