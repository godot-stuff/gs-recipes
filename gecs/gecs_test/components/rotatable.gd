extends Node

export (int) var STARTING_ROTATION = 0
export (int) var SPEED = 5
export (int) var SPEED_FACTOR = 10
export (bool) var SMOOTHING = false
export (float) var SMOOTHING_FACTOR = 1.0

var rotation = 0
var speed = 5
var speed_factor = 10
var smoothing = false
var smoothing_factor = 1.0

func _ready():

	if SPEED:				speed = SPEED
	if SPEED_FACTOR:		speed_factor = SPEED_FACTOR
	if SMOOTHING:			smoothing = SMOOTHING
	if SMOOTHING_FACTOR:	smoothing_factor = SMOOTHING_FACTOR

	if STARTING_ROTATION:	rotation = STARTING_ROTATION