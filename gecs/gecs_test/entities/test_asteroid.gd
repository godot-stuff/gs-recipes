extends "res://gecs/entity.gd"


func on_ready():

	TestGame.emit_signal("asteroid_created")

	var ANGLE = floor(rand_range(0, 360))
	components['velocity'].direction = Vector2(cos(deg2rad(ANGLE-90)), sin(deg2rad(ANGLE-90)))

	var dir = [1, -1]
	components['rotating'].direction = dir[floor(randi() % 2)]
	components['rotating'].rotation = floor(randi() % 360)


