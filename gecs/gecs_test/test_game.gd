extends Node

enum {
	STATE_NONE,
	STATE_RUN,
	STATE_READY,
	STATE_OVER,
	STATE_SCORE,
	STATE_INTRO,
	STATE_PAUSE,
	STATE_WIN
}


signal add_score(points)
signal player_dead()
signal asteroid_destroyed()
signal asteroid_created()
signal game_state_changed(state)

var score = 0
var asteroids = 0
var lives = 3

var state = 0


func change_state(state_):
	state = state_
	TestGame.emit_signal("game_state_changed", state)


func _on_player_dead():
	lives -= 1

	if lives > 0:
		change_state(STATE_READY)
		return

	change_state(STATE_OVER)

func _on_asteroid_destroyed():
	asteroids -= 1
	if asteroids <= 0:
		change_state(STATE_WIN)


func _on_asteroid_created():
	print("asteroid created")
	asteroids += 1


func _on_add_score(points):
	score += points


func _ready():
	connect("add_score", self, "_on_add_score")
	connect("asteroid_destroyed", self, "_on_asteroid_destroyed")
	connect("asteroid_created", self, "_on_asteroid_created")
	connect("player_dead", self, "_on_player_dead")
	change_state(STATE_NONE)


func _init():
	randomize()