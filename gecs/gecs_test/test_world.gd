extends Node


export (bool) var DEBUG = true


const asteriod = preload("res://gecs_test/entities/test_asteroid.tscn")


func _debug():

	if not DEBUG:
		return

	var player = $TestPlayer.components['player']

	var debug_text = \
	"""entities: %s
	fps: %s
	score: %s
	lives: %s
	player dead: %s
	asteroids: %s
	state: %s
	"""

	$Debug/Container/Label.text = debug_text % [
		gecs._entities.size(),
		Engine.get_frames_per_second(),
		TestGame.score,
		TestGame.lives,
		player.dead,
		TestGame.asteroids,
		TestGame.state
		]


func _hide_player():
	$TestPlayer.hide()


func _on_game_state_change(state):

	$TestPlayer.hide()

	$UI/Intro.hide()
	$UI/Game.hide()
	$UI/Ready.hide()
	$UI/Over.hide()
	$UI/Score.hide()
	$UI/Win.hide()

	match TestGame.state:

		TestGame.STATE_NONE:
			pass

		TestGame.STATE_READY:
			_reset_level()
			$UI/Ready.show()
			$UI/Game.show()
			$UI/Ready/ReadyAnimation.play("flash")

		TestGame.STATE_INTRO:
			$UI/Intro.show()

		TestGame.STATE_RUN:
			$UI/Game.show()
			$TestPlayer.show()

		TestGame.STATE_OVER:
			$UI/Game.show()
			$UI/Over.show()

		TestGame.STATE_WIN:
			$TestPlayer.enabled = false
			$TestPlayer.hide()
			$UI/Win.show()
			$UI/Game.show()



func _reset_level():

	for entity in $Asteroids.get_children():
		gecs.remove_entity(entity)

	TestGame.asteroids = 0

	var a = asteriod.instance()
	a.position = Vector2(480, 75)
	$Asteroids.add_child(a)
	a = asteriod.instance()
	a.position = Vector2(100, 150)
	$Asteroids.add_child(a)
	a = asteriod.instance()
	a.position = Vector2(140, 850)
	$Asteroids.add_child(a)

	$TestPlayer.enabled = true
	$TestPlayer.components['player'].dead = false
	$TestPlayer.position = Vector2(270, 480)


func _reset_game():

	TestGame.lives = 3
	TestGame.score = 0

func _on_Pause_pressed():
	if TestGame.state == TestGame.STATE_RUN:
		TestGame.change_state(TestGame.STATE_PAUSE)
	else:
		TestGame.change_state(TestGame.STATE_RUN)


func _on_ReadyAnimation_animation_finished(anim_name):
	TestGame.change_state(TestGame.STATE_RUN)


func _on_Play_pressed():
	_reset_game()
	TestGame.change_state(TestGame.STATE_READY)


func _process(delta):


	_debug()

	match TestGame.state:

		TestGame.STATE_NONE:
			TestGame.change_state(TestGame.STATE_INTRO)

		TestGame.STATE_INTRO:
			gecs.update("CommonWorld", delta)
			gecs.update("IntroWorld", delta)

		TestGame.STATE_RUN:
			gecs.update("CommonWorld", delta)
			gecs.update("GameWorld", delta)

		TestGame.STATE_PAUSE:
			gecs.update("PauseWorld", delta)

		TestGame.STATE_OVER:
			gecs.update("CommonWorld", delta)
			gecs.update("OverWorld", delta)

		TestGame.STATE_WIN:
			gecs.update("CommonWorld", delta)
			gecs.update("OverWorld", delta)


func _ready():
	TestGame.connect("game_state_changed", self, "_on_game_state_change")


func _init():
	pass


func _on_Debug_pressed():
	DEBUG = !DEBUG
	$Debug/Container/Label.hide()
	if DEBUG:
		$Debug/Container/Label.show()
