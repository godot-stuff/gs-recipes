extends Node


var enabled = true
var id setget , _get_id
var components = {}


func on_init():
	pass


func on_ready():
	pass


func on_reset():
	pass


func component(name_):
	if components.has(name_):
		return components[name_]
	return null


func _get_id():
	return get_instance_id()


func _ready():
	gecs.add_entity(self)
	on_ready()

func _init():
	on_init()


