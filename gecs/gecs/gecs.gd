extends Node

var _entities = {}
var _entities_components = {}

var _components = {}
#var _component_entities = {}

var _filters
#var _filter_entities

var _systems = {}
var _systems_components = {}
#var _system_entities = {}

var _worlds = {}
var _world_systems = {}
var _world_system_components = {}

var _delete_queue = {}


#	rebuild the entity system database
func rebuild():
	pass

#	add a new entity to the current world
func add_entity(entity_):

	var _components = []
	for child in entity_.find_node("Components").get_children():
		entity_.components[child.name.to_lower()] = child
		_components.append(child)

	_entities[entity_.get_instance_id()] = entity_
	_entities_components[entity_.get_instance_id()] = []

	var names = []
	for component in _components:
		_entities_components[entity_.get_instance_id()].append(component.name.to_lower())

#	for component in _components:
#		var key = component.name.to_lower()
#		if not _components.has(key):
#			_components[key] = component
#			_component_entities[key] = []
#		_component_entities[key].append(entity_.get_instance_id())


func add_world(world_):

	_worlds[world_.name] = world_

	var systems = []
	for child in world_.get_children():
		systems.append(child)

	_world_systems[world_.name] = systems

	for system in systems:
		_world_system_components[world_.name + system.name] = system.components



#	schedule an entity to be removed from the current world
func remove_entity(entity_):

	entity_.enabled = false
	_delete_queue[entity_.id] = entity_


#	add a new component to the active world
#	components with the same name will be reused
#	does not check for uniqueness (name clashes)
func add_component(name_):
	_components[name_] = name_


#	add a new system to the active world
#	does not check for uniquness (name clashes)
func add_system(system_, components_):

	return

	_systems[system_.name] = system_
	_systems_components[system_.name] = components_


#	given a system name, return a list of entities for processing
#	much faster than adhoc filtering
#	better memory management with pooling
#func get_system_entities(system_):
#	pass


#	create a custom filter of entities
#	all components must exist
#	must be rebuilt after adding entities
#	not a stored entity list
func create_filter(name_, components):
	pass

#	clear the current world (bad idea)
func clear():
	_components = {}
	_systems = {}
	_systems_components = {}
	_entities = {}


#	return an array of entities based on the passed components
#	very slow
#	memory fragmentation
#	use get_system_entities() instead
func _filter_entities(components_):

	var entities = []

	for entity_key in _entities:

		if not _entities[entity_key].enabled:
			continue

		var has_all_components = true
		for component in components_:

			if not _entities_components[entity_key].has(component):
				has_all_components = false
				break

		if has_all_components:
			entities.append(_entities[entity_key])

#	for i in _component_entities['position']:
#		entities.append(_entities[i])

	return entities

func update(world_, delta_):

	if not _entities:
		return

#	_systems.keys().sort()

#	for s in _systems:
#
#		if _systems[s].enabled:
#			_systems[s].process(_filter_entities(_systems_components[s]), delta)
#			_systems[s].process(_entities.values(), delta)

	for system in _world_systems[world_]:
		var entities = _filter_entities(_world_system_components[world_+system.name])
		system.process(entities, delta_)

	# remove entities queued for removal

	if _delete_queue.size() > 0:
		for entity in _delete_queue:
			if _entities.has(entity):
				_entities.erase(entity)
				_delete_queue[entity].queue_free()

	_delete_queue.clear()