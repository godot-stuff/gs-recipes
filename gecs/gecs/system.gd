extends Node

export (String) var COMPONENTS = ""
export (bool) var ENABLED_ = true
export (bool) var AWAKE_ = true

var enabled = false
var awake = false
var components = []

func on_init():
	pass


func on_ready():
	pass


func on_reset():
	pass


func _ready():

	if ENABLED_:
		enabled = ENABLED_

	if AWAKE_:
		awake = AWAKE_

	if COMPONENTS:
		components = COMPONENTS.split(",")

	on_ready()