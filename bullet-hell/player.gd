extends Area2D

var hits = 0

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass



func _on_Player_body_entered(body):
	hits += 1
	print("body ouch", hits)


func _on_Player_area_entered(area):
	hits += 1
	print("area ouch", hits)


func _on_Player_body_shape_entered(body_id, body, body_shape, area_shape):
	hits += 1
	print("shape ouch", hits)
