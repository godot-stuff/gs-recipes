extends Node2D

export (PackedScene) var SCENE
export (bool) var USE_VIEWPORT
export (Vector2) var TOP_LEFT
export (Vector2) var BOTTOM_RIGHT
export (int) var POOL_SIZE = 750
export (int) var BURST_SIZE = 20
export (int) var ROTATION = 1
export (float) var DELAY = 0.5


var bullets = []
var pool = []
var alive = {}
var count = 0
var shape
var r = 0
onready var texture = preload("res://laser.png")

class Bullet:
	var node
	var pos = Vector2()
	var speed = 35
	var speed_factor = 10
	var body = RID()
	var dir = Vector2()
	var index = -1


func _process(delta):

	$CanvasLayer/DEBUG.text = "bullets: %s \nfps: %s" % [alive.size(), str(Engine.get_frames_per_second())]

	$Enemy/Emitter.rotation += ROTATION * delta

	for b in alive.values():

		var remove = false

		b.node.position += b.dir * b.speed * b.speed_factor * delta

		if b.node.position.x < TOP_LEFT.x:
			remove = true

		if b.node.position.x > BOTTOM_RIGHT.x:
			remove = true

		if b.node.position.y < TOP_LEFT.y:
			remove = true

		if b.node.position.y > BOTTOM_RIGHT.y:
			remove = true

		if remove:
			b.node.visible = false
			b.node.set_process(false)
			pool.append(b)
			alive.erase(b.index)
			continue



func _add_bullet():
	var bullet = Bullet.new()
	bullet.node = SCENE.instance()
	bullet.node.visible = false
	$Bullets.add_child(bullet.node)
	return bullet


func _get_open_bullet():
	var bullet = pool.pop_back()
	if bullet:
		bullet.node.visible = true
		bullet.node.set_process(true)
		return bullet

func _ready():

	if USE_VIEWPORT:
		TOP_LEFT = Vector2(0,0)
		BOTTOM_RIGHT = get_viewport().get_visible_rect().size

	for i in range(POOL_SIZE):
		var b = _add_bullet()
		b.index = i
		pool.append(b)
		bullets.append(b)

	$Timer.wait_time = DELAY


func _on_Timer_timeout():
	for i in range(BURST_SIZE):
		var bullet = _get_open_bullet()
		if bullet:
			bullet.node.position = $Enemy/Emitter.global_position
			var _a = $Enemy/Emitter.rotation + i*10
			var _dir = Vector2(cos(deg2rad(_a)), sin(deg2rad(_a)))
			bullet.dir = _dir
			alive[bullet.index] = bullet

