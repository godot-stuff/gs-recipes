extends Node2D

export(int) var bullet_count = 20
export (bool) var USE_VIEWPORT
export (Vector2) var TOP_LEFT
export (Vector2) var BOTTOM_RIGHT


var bullets = []
var shape
onready var texture = preload("res://laser.png")

class Bullet:
	var pos = Vector2()
	var speed = 15
	var speed_factor = 10
	var body = RID()
	var dir = Vector2()
	var purge = false


func _draw():

	for b in bullets:
		draw_set_transform(b.pos, 0, Vector2(0.5, 0.5))
		draw_texture(texture, b.pos)


func _process(delta):

	$CanvasLayer/DEBUG.text = "bullets: %s \nfps: %s" % [bullets.size(), str(Engine.get_frames_per_second())]

	var t = Transform2D()

	for b in bullets:

		var remove = false

		b.pos += b.dir * b.speed * b.speed_factor * delta

#		if b.pos.x < TOP_LEFT.x:
#			b.pos.x = BOTTOM_RIGHT.x
#
#		if b.pos.x > BOTTOM_RIGHT.x:
#			b.pos.x = TOP_LEFT.x
#
#		if b.pos.y < TOP_LEFT.y:
#			b.pos.y = BOTTOM_RIGHT.y
#
#		if b.pos.y > BOTTOM_RIGHT.y:
#			b.pos.y = TOP_LEFT.y

		if b.pos.x < TOP_LEFT.x:
			remove = true

		if b.pos.x > BOTTOM_RIGHT.x:
			remove = true

		if b.pos.y < TOP_LEFT.y:
			remove = true

		if b.pos.y > BOTTOM_RIGHT.y:
			remove = true

		if remove:
			b.purge = true
			continue

		t.origin = b.pos
		Physics2DServer.body_set_state(b.body, Physics2DServer.BODY_STATE_TRANSFORM, t)

	var temp = []
	for i in range(bullets.size()):
		var bullet = bullets[i]
		if bullet.purge:
			Physics2DServer.free_rid(bullet.body)
		else:
			temp.append(bullet)

	bullets = temp

	update()


func _add_bullet():
	var bullet = Bullet.new()
	bullet.body = Physics2DServer.body_create()
	Physics2DServer.body_set_mode(bullet.body, Physics2DServer.BODY_MODE_KINEMATIC)
	Physics2DServer.body_set_space(bullet.body, get_world_2d().get_space())
	Physics2DServer.body_add_shape(bullet.body, shape)
	bullet.pos = Vector2(480, 270)
	var t = Transform2D()
	t.origin = bullet.pos
	Physics2DServer.body_set_state(bullet.body, Physics2DServer.BODY_STATE_TRANSFORM, t)
	var _a = floor(rand_range(0, 360))
	var _dir = Vector2(cos(deg2rad(_a-90)), sin(deg2rad(_a-90)))
	bullet.dir = _dir
	return bullet

func _ready():

	if USE_VIEWPORT:
		TOP_LEFT = Vector2(0,0)
		BOTTOM_RIGHT = get_viewport().get_visible_rect().size

	shape = Physics2DServer.circle_shape_create()
	Physics2DServer.shape_set_data(shape, 1) # radius

#	for i in range(bullet_count):
#		bullets.append(_add_bullet())




func _on_Timer_timeout():
	for i in range(bullet_count):
		bullets.append(_add_bullet())

