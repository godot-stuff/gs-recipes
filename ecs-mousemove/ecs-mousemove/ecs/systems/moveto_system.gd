
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MoveToSystem
extends System

func on_process_entity(entity : Entity, delta : float) -> void:
	
	var _moveto = entity.get_component('moveto') as MoveToComponent
	
	if (_moveto.is_moving):
		
		var _dist = entity.global_position.distance_to(_moveto.next_position)
		print(_dist)
		
		var _speed = _moveto.speed * _moveto.speed_factor * delta
		
		if (_moveto.move_type == _moveto.MOVETO_TYPE.MOVETO_TYPE_LINEAR):
			entity.global_position = entity.global_position.move_toward(_moveto.next_position, _speed)
			_moveto.is_moving = true
		
		if (_moveto.move_type == _moveto.MOVETO_TYPE.MOVETO_TYPE_LERP):
#			print(lerp(entity.global_position, Global.selected_space.global_position, delta))
			entity.global_position = lerp(entity.global_position, _moveto.next_position, (_speed / 100))

		if (_dist < 0.5):
			entity.global_position = _moveto.next_position
			_moveto.curr_position = _moveto.next_position
			_moveto.is_moving = false
		
	
