
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name SelectedSpaceSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	if (Global.selected_space):
		entity.global_position = Global.selected_space.global_position
