
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

#
# Returns the Global Position of the
# mouse cursor relative to the Screen
#

class_name MousePositionSystem
extends System


func on_process_entity(entity : Entity, delta : float):
	var _mouse = entity.get_component('mouse') as MouseComponent
	_mouse.mouse_position = get_viewport().get_mouse_position()
	print(_mouse.mouse_position)
