
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MouseGridPositionSystem
extends System


func on_process_entity(entity : Entity, delta : float) -> void:
	
	var _grid_component = entity.get_component('grid')
