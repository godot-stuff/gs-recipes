
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name LerpToSystem
extends System

func on_process_entity(entity : Entity, delta : float) -> void:
	
	var _moveto = entity.get_component('moveto') as MoveToComponent
	
	if (Global.selected_space):
		var _speed = _moveto.speed * _moveto.speed_factor * delta
		entity.global_position = entity.global_position.move_toward(Global.selected_space.global_position, _speed)
		
#		print(lerp(entity.global_position, Global.selected_space.global_position, delta))
#		entity.global_position = lerp(entity.global_position, Global.selected_space.global_position, _speed / 10)
	
