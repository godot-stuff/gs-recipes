
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name CurrentSpaceSystem
extends System


func on_process_entity(entity : Entity, delta : float):
	
	if (Global.current_space):
		entity.global_position = Global.current_space.global_position
