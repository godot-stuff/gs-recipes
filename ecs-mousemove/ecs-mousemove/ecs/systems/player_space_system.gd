
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name PlayerSpaceSystem
extends System


func on_process_entity(entity : Entity, delta : float):
	
	var _moveto = entity.get_component("moveto") as MoveToComponent
	
	if (Global.selected_space):
		_moveto.next_space = Global.selected_space
		_moveto.last_space = _moveto.curr_space
		_moveto.next_position = Global.selected_space.global_position
		_moveto.is_moving = true
		Global.beacon_active = false
		Global.selected_space = null
		
	if (! _moveto.is_moving):
		_moveto.curr_space = _moveto.next_space
		Global.beacon_active = true
		Global.player_space = _moveto.curr_space
		
		
