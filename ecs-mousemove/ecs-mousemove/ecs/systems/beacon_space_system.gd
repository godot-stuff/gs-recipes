
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name BeaconSpaceSystem
extends System


func on_process_entity(entity : Entity, delta : float):

	var _moveto = entity.get_component("moveto") as MoveToComponent
	
	if (Global.beacon_space):
		_moveto.next_space = Global.beacon_space
		_moveto.last_space = _moveto.curr_space
		_moveto.next_position = Global.beacon_space.global_position
		_moveto.is_moving = true
		Global.beacon_space = null
		Global.beacon_active = false
