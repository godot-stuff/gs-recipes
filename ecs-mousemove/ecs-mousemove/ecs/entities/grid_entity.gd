
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name GridEntity
extends Entity

export var grid_width : int = 10
export var grid_height : int = 10
export var grid_offset : Vector2 
export var tile_size : int = 64
export var tile_scene : PackedScene

func _physics_process(delta):
	pass
	
func _on_mouse_enter_space(tile : GameTile):
	Global.current_space = tile
	
	
func _on_input_event( viewport : Node, event : InputEvent, shape_idx : int, tile : GameTile):
	
	if Input.is_action_pressed("game_action"):
		Global.selected_space = tile
		print(tile)
	
	if (Global.beacon_active):
		if (event.is_action_pressed("game_beacon")):
			Global.beacon_space = Global.player_space
	
	
func on_ready():
	
	set_process(true)
	
	var _root = $Root
	_root.global_position = grid_offset + (Vector2.ONE * (tile_size / 2))
	
	for w in range(grid_width):
		for h in range(grid_height):
			var _tile = tile_scene.instance() as GameTile
			_root.add_child(_tile)
			_tile.position = Vector2(w, h) * tile_size
			_tile.connect("mouse_entered", self, "_on_mouse_enter_space", [_tile])
			_tile.connect("input_event", self, "_on_input_event", [_tile])
			_tile.tile_space = Vector2(w, h)
