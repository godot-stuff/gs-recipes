
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MoveToComponent
extends Component

enum MOVETO_TYPE \
{
	MOVETO_TYPE_NONE,	
	MOVETO_TYPE_LINEAR,
	MOVETO_TYPE_LERP,
}

export var speed : float = 10.0
export var speed_factor : float = 1.0
export (MOVETO_TYPE) var move_type = MOVETO_TYPE.MOVETO_TYPE_LINEAR

var next_position : Vector2
var curr_position : Vector2
var last_position : Vector2

var next_space : GameTile
var curr_space : GameTile
var last_space : GameTile

var is_moving : bool = false


func get_name():
	return 'moveto'
	

