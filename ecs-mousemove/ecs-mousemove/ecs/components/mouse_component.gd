
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MouseComponent
extends Component

var mouse_position : Vector2

func get_name():
	return "mouse"
