
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

class_name MoveComponent
extends Component

export var speed : float = 1.0
export var speed_factor : int = 10

var is_moving : bool = false
