
#	Copyright 2020, SpockerDotNet LLC - All Rights Reserved
#	https://sdnllc.com

extends Node2D

# size of each grid tile
export var grid_size : int = 64

# the space that has focus - eg: keyboard or mouse movement
var current_space : GameTile

# the space that is selected - eg: pressed mouse button or pressed enter
var selected_space : GameTile

# the space that is selected for the beacon
var beacon_space : GameTile

# the space the player is in
var player_space : GameTile

var beacon_active : bool = false
