extends Node


func _on_got_scores(scores):
	for score in scores:
		$Label.text += "%s (%s)" % [score.name, score.score]
		
		
func _ready():
	DreamLo.get_scores()
	
	
func _init():
	DreamLo.connect(DreamLo.DREAMLO_GOT_SCORES, self, "_on_got_scores")
	
	