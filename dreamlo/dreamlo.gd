extends Node

signal dreamlo_request

const DREAMLO_PLAYER_ADD_SCORE = "/add/{player}/{score}"
const DREAMLO_PLAYER_ADD_SCORE_TIME = "/add/{player}/{score}/{time}"
const DREAMLO_PLAYER_ADD_SCORE_TIME_MESSAGE = "/add/{player}/{score}/{time}/{message}"
const DREAMLO_PLAYER_DELETE = "/delete/{player}"
const DREAMLO_CLEAR_SCORES = "/clear"
const DREAMLO_GET_SCORES = "/{format}"
const DREAMLO_GET_SCORES_TOP = "/{format}/{top}"
const DREAMLO_GET_SCORES_RANGE = "/{format}/{start}/{count}"
const DREAMLO_GET_SCORES_PLAYER = "/{format}-get/{player}"

var uri
var url
var private_code
var public_code
var queue = Array()
var http_request

func add_score(player, score):
	_submit_request(_build_submit_request(DREAMLO_PLAYER_ADD_SCORE.format({ "player": player, "score": score })))
	
	
func delete_score(player):
	_submit_request(_build_submit_request(DREAMLO_PLAYER_DELETE.format({ "player": player })))

	
func clear_scores():
	_submit_request(_build_submit_request(DREAMLO_CLEAR_SCORES))
	
	
func get_scores():
	_submit_request(_build_submit_request(DREAMLO_GET_SCORES.format({ "format": "json" })))
	
		
func _build_submit_request(command):
	return _build_request(uri, private_code, command)
	
	
func _build_get_request(command):
	pass
	
	
func _build_request(uri, code, command):
	return "{uri}/{code}{command}".format({ "uri": uri, "code": code, "command": command })
	
func _submit_request(request):
	print("_submit_request", request)
	queue.push_front(request)
#	print(queue)
#	print($HTTPRequest.get_http_client_status())
	if http_request.get_http_client_status() == HTTPClient.STATUS_DISCONNECTED:
		http_request.request(queue.pop_back())
	
func _test():
		clear_scores()
		add_score("paul", 1000)
		add_score("paul", 3000)
		add_score("simon", 2000)
		get_scores()		

func _init():
	print("dreamlo:_init")
	http_request = HTTPRequest.new()
	http_request.name = "_http_request"
	add_child(http_request)
	http_request.connect("request_completed", self, "_on_request_completed")
	
	
func _ready():
	print("dreamlo:_ready")
	var config = _load_config()
	if config:
		uri = config.get_value("dreamlo", "uri")
		url = config.get_value("dreamlo", "url")
		private_code = config.get_value("dreamlo", "private_code")
		public_code = config.get_value("dreamlo", "public_code")
		_test()
	else:
		print("bummer")

func _load_config():
	print("dreamlo:_load_config")
	var _config = ConfigFile.new()
	var _config_filename = "res://dreamlo.cfg"
	
	if ProjectSettings.has_setting("game/dreamlo_config"):
		_config_filename = ProjectSettings.get("game/dreamlo_config")

	var file = File.new()
	if not file.file_exists(_config_filename):
		print("Warning:DreamLo Configuration File {name} is missing.".format({"name": _config_filename}))
		return
				
	print(_config_filename)
	var _err = _config.load(_config_filename)
	return _config

func _on_request_completed(result, response_code, headers, body):
#	print(result, response_code, headers, body.get_string_from_ascii())
	emit_signal("dreamlo_request", { "result": result, "response_code": response_code, "headers:": headers, "body": body})
	if !queue.empty():
		http_request.request(queue.pop_back())
