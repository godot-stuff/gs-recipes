extends Label

onready var dreamlo = get_node("..")

func _ready():
	dreamlo.connect("dreamlo_request", self, "_on_dreamlo_request")
	
func _on_dreamlo_request(package):
	print("on_dreamlo_request")
	print(package.body.get_string_from_ascii())