extends Node2D

onready var Easing = preload("res://new_easing.gd")
onready var panel = $Panel

export var START_VALUE = Vector2(100,100)
export var END_VALUE = Vector2(0, 400)
export var DURATION: float = 5.0
export var FRAMES: int = 4
export(float, EASE) var transition_speed


var start_value
var end_value
var duration
var change
var time_elapsed
var offset_x: float
var offset_y: float
var active = false
var frame_num = 0
var click_start: Vector2
var last_pos: Vector2 = Vector2(100,100)
var next_pos: Vector2

var easing_x: int
var easing_y: int

var offset_pos = []

func _draw() -> void:

	if start_value:
		draw_circle(start_value, 25.0, ColorN("white", .25))

	if offset_pos:
		for o in offset_pos:
			draw_circle(o, 25.0, ColorN("white", 0.25))

	draw_circle(Vector2(offset_x, offset_y), 25.0, ColorN("darkslategray", .75))


func _process(delta: float) -> void:

	if $Options.show:
		return

	if Input.is_action_just_pressed("action"):
		active = true
		time_elapsed = 0
		start_value = Vector2(offset_x, offset_y)
		next_pos = get_global_mouse_position()
		end_value =  next_pos - start_value
		offset_pos = []

	if !active:
		return

	time_elapsed += delta

#	print(time_elapsed)

	if time_elapsed > duration:
		time_elapsed = 0
		active = false
		last_pos = next_pos
		offset_x = last_pos.x
		offset_y = last_pos.y
		offset_pos.append(last_pos)
		update()
		return

	offset_x = _update_offset(easing_x, time_elapsed, start_value.x, end_value.x, duration)
	offset_y = _update_offset(easing_y, time_elapsed, start_value.y, end_value.y, duration)

	frame_num += 1

	if frame_num > FRAMES:
		frame_num = 0
		offset_pos.append(Vector2(offset_x, offset_y))

	update()


func _update_offset(easing, t, s, e, d):

	match easing:

		Easing.LINEAR_EASE_NONE:	return Easing.Linear.easeNone(t, s, e, d)
		Easing.LINEAR_EASE_IN:		return Easing.Linear.easeIn(t, s, e, d)
		Easing.LINEAR_EASE_OUT:		return Easing.Linear.easeOut(t, s, e, d)
		Easing.LINEAR_EASE_INOUT:	return Easing.Linear.easeInOut(t, s, e, d)
		Easing.QUAD_EASE_IN:		return Easing.Quad.easeIn(t, s, e, d)
		Easing.QUAD_EASE_OUT:		return Easing.Quad.easeOut(t, s, e, d)
		Easing.QUAD_EASE_INOUT:		return Easing.Quad.easeInOut(t, s, e, d)
		Easing.SIN_EASE_IN:			return Easing.Sine.easeIn(t, s, e, d)
		Easing.SIN_EASE_OUT:		return Easing.Sine.easeOut(t, s, e, d)
		Easing.SIN_EASE_INOUT:		return Easing.Sine.easeInOut(t, s, e, d)
		Easing.EXPO_EASE_IN:		return Easing.Expo.easeIn(t, s, e, d)
		Easing.EXPO_EASE_OUT:		return Easing.Expo.easeOut(t, s, e, d)
		Easing.EXPO_EASE_INOUT:		return Easing.Expo.easeInOut(t, s, e, d)
		Easing.CIRC_EASE_IN:		return Easing.Circ.easeIn(t, s, e, d)
		Easing.CIRC_EASE_OUT:		return Easing.Circ.easeOut(t, s, e, d)
		Easing.CIRC_EASE_INOUT:		return Easing.Circ.easeInOut(t, s, e, d)
		Easing.CUBE_EASE_IN:		return Easing.Cubic.easeIn(t, s, e, d)
		Easing.CUBE_EASE_OUT:		return Easing.Cubic.easeOut(t, s, e, d)
		Easing.CUBE_EASE_INOUT:		return Easing.Cubic.easeInOut(t, s, e, d)
		Easing.QUART_EASE_IN:		return Easing.Quart.easeIn(t, s, e, d)
		Easing.QUART_EASE_OUT:		return Easing.Quart.easeOut(t, s, e, d)
		Easing.QUART_EASE_INOUT:	return Easing.Quart.easeInOut(t, s, e, d)
		Easing.QUINT_EASE_IN:		return Easing.Quint.easeIn(t, s, e, d)
		Easing.QUINT_EASE_OUT:		return Easing.Quint.easeOut(t, s, e, d)
		Easing.QUINT_EASE_INOUT:	return Easing.Quint.easeInOut(t, s, e, d)
		Easing.ELASTIC_EASE_IN:		return Easing.Elastic.easeIn(t, s, e, d)
		Easing.ELASTIC_EASE_OUT:	return Easing.Elastic.easeOut(t, s, e, d)
		Easing.ELASTIC_EASE_INOUT:	return Easing.Elastic.easeInOut(t, s, e, d)
		Easing.BOUNCE_EASE_IN:		return Easing.Bounce.easeIn(t, s, e, d)
		Easing.BOUNCE_EASE_OUT:		return Easing.Bounce.easeOut(t, s, e, d)
		Easing.BOUNCE_EASE_INOUT:	return Easing.Bounce.easeInOut(t, s, e, d)
		Easing.BACK_EASE_IN:		return Easing.Back.easeIn(t, s, e, d)
		Easing.BACK_EASE_OUT:		return Easing.Back.easeOut(t, s, e, d)
		Easing.BACK_EASE_INOUT:		return Easing.Back.easeInOut(t, s, e, d)

		_:							return Easing.Linear.easeNone(t, s, e, d)


func _on_change_easing_x(easing):
	easing_x = easing


func _on_change_easing_y(easing):
	easing_y = easing


func _ready() -> void:

	start_value = START_VALUE
	end_value = END_VALUE
	duration = DURATION
	active = false
	time_elapsed = 0
	offset_pos = []
	offset_x = last_pos.x
	offset_y = last_pos.y

	$Options.connect("change_easing_x", self, "_on_change_easing_x")
	$Options.connect("change_easing_y", self, "_on_change_easing_y")

	easing_x = Easing.LINEAR_EASE_NONE
	easing_y = Easing.LINEAR_EASE_NONE

#	VisualServer.set_default_clear_color(ColorN("midnightblue"))
