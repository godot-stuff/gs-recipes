extends Node2D

onready var Easing = preload("res://new_easing.gd")


export var START_VALUE = Vector2(100,100)
export var END_VALUE = Vector2(100, 600)
export var DURATION: float = 3.0

var time_elapsed
var next_value

func _process(delta: float) -> void:

	if time_elapsed > DURATION:
		return

	time_elapsed += delta

	var offset = Easing.Linear.easeOut(time_elapsed, START_VALUE, next_value, DURATION)

	$Sprite.position = offset

func _ready():
	next_value = END_VALUE - START_VALUE
	time_elapsed = 0

