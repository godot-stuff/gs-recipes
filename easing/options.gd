extends Node

onready var Easing = preload("res://new_easing.gd")
onready var panel:Panel = $Panel

signal change_easing_x
signal change_easing_y



var show: bool = false
var tween: Tween

var top_pos: Vector2 = Vector2(10, -500)
var bot_pos: Vector2 = Vector2(10, 0)


func _process(delta: float) -> void:

	if Input.is_action_just_pressed("menu"):

		show = !show

		if tween.is_active():
			tween.stop_all()

		var next_pos = top_pos

		if show:
			next_pos = bot_pos


		tween.interpolate_property(panel, "rect_global_position", panel.rect_global_position, next_pos, 0.5, Tween.TRANS_EXPO, Tween.EASE_OUT)

		tween.start()



func _ready() -> void:

	tween = Tween.new()
	add_child(tween)

	_clear_x()
	$Panel/GridContainer/CheckBox01.pressed = true
	emit_signal("change_easing_x", Easing.LINEAR_EASE_IN)

	_clear_y()
	$Panel/GridContainer/CheckBox02.pressed = true
	emit_signal("change_easing_y", Easing.LINEAR_EASE_IN)


func _clear_x():
	$Panel/GridContainer/CheckBox01.pressed = false
	$Panel/GridContainer/CheckBox03.pressed = false
	$Panel/GridContainer/CheckBox05.pressed = false
	$Panel/GridContainer/CheckBox07.pressed = false
	$Panel/GridContainer/CheckBox09.pressed = false
	$Panel/GridContainer/CheckBox11.pressed = false
	$Panel/GridContainer/CheckBox13.pressed = false
	$Panel/GridContainer/CheckBox15.pressed = false
	$Panel/GridContainer/CheckBox17.pressed = false
	$Panel/GridContainer/CheckBox19.pressed = false
	$Panel/GridContainer/CheckBox21.pressed = false
	$Panel/GridContainer/CheckBox23.pressed = false
	$Panel/GridContainer/CheckBox25.pressed = false
	$Panel/GridContainer/CheckBox27.pressed = false
	$Panel/GridContainer/CheckBox29.pressed = false
	$Panel/GridContainer/CheckBox31.pressed = false
	$Panel/GridContainer/CheckBox33.pressed = false
	$Panel/GridContainer/CheckBox35.pressed = false
	$Panel/GridContainer/CheckBox37.pressed = false
	$Panel/GridContainer/CheckBox39.pressed = false
	$Panel/GridContainer/CheckBox41.pressed = false
	$Panel/GridContainer/CheckBox43.pressed = false
	$Panel/GridContainer/CheckBox45.pressed = false
	$Panel/GridContainer/CheckBox47.pressed = false
	$Panel/GridContainer/CheckBox01.pressed = false
	$Panel/GridContainer/CheckBox03.pressed = false
	$Panel/GridContainer/CheckBox05.pressed = false
	$Panel/GridContainer/CheckBox07.pressed = false
	$Panel/GridContainer/CheckBox09.pressed = false
	$Panel/GridContainer2/CheckBox2_01.pressed = false
	$Panel/GridContainer2/CheckBox2_03.pressed = false
	$Panel/GridContainer2/CheckBox2_05.pressed = false
	$Panel/GridContainer2/CheckBox2_07.pressed = false
	$Panel/GridContainer2/CheckBox2_09.pressed = false
	$Panel/GridContainer2/CheckBox2_11.pressed = false
	$Panel/GridContainer2/CheckBox2_13.pressed = false
	$Panel/GridContainer2/CheckBox2_15.pressed = false
	$Panel/GridContainer2/CheckBox2_17.pressed = false

func _clear_y():
	$Panel/GridContainer/CheckBox02.pressed = false
	$Panel/GridContainer/CheckBox04.pressed = false
	$Panel/GridContainer/CheckBox06.pressed = false
	$Panel/GridContainer/CheckBox08.pressed = false
	$Panel/GridContainer/CheckBox10.pressed = false
	$Panel/GridContainer/CheckBox12.pressed = false
	$Panel/GridContainer/CheckBox14.pressed = false
	$Panel/GridContainer/CheckBox16.pressed = false
	$Panel/GridContainer/CheckBox18.pressed = false
	$Panel/GridContainer/CheckBox20.pressed = false
	$Panel/GridContainer/CheckBox22.pressed = false
	$Panel/GridContainer/CheckBox24.pressed = false
	$Panel/GridContainer/CheckBox26.pressed = false
	$Panel/GridContainer/CheckBox28.pressed = false
	$Panel/GridContainer/CheckBox30.pressed = false
	$Panel/GridContainer/CheckBox32.pressed = false
	$Panel/GridContainer/CheckBox34.pressed = false
	$Panel/GridContainer/CheckBox36.pressed = false
	$Panel/GridContainer/CheckBox38.pressed = false
	$Panel/GridContainer/CheckBox40.pressed = false
	$Panel/GridContainer/CheckBox42.pressed = false
	$Panel/GridContainer/CheckBox44.pressed = false
	$Panel/GridContainer/CheckBox46.pressed = false
	$Panel/GridContainer/CheckBox48.pressed = false
	$Panel/GridContainer/CheckBox02.pressed = false
	$Panel/GridContainer/CheckBox04.pressed = false
	$Panel/GridContainer/CheckBox06.pressed = false
	$Panel/GridContainer/CheckBox08.pressed = false
	$Panel/GridContainer/CheckBox10.pressed = false
	$Panel/GridContainer2/CheckBox2_02.pressed = false
	$Panel/GridContainer2/CheckBox2_04.pressed = false
	$Panel/GridContainer2/CheckBox2_06.pressed = false
	$Panel/GridContainer2/CheckBox2_08.pressed = false
	$Panel/GridContainer2/CheckBox2_10.pressed = false
	$Panel/GridContainer2/CheckBox2_12.pressed = false
	$Panel/GridContainer2/CheckBox2_14.pressed = false
	$Panel/GridContainer2/CheckBox2_16.pressed = false
	$Panel/GridContainer2/CheckBox2_18.pressed = false


func _on_CheckBox01_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox01.pressed = true
	emit_signal("change_easing_x", Easing.LINEAR_EASE_IN)


func _on_CheckBox02_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox02.pressed = true
	emit_signal("change_easing_y", Easing.LINEAR_EASE_IN)


func _on_CheckBox03_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox03.pressed = true
	emit_signal("change_easing_x", Easing.QUAD_EASE_IN)


func _on_CheckBox04_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox04.pressed = true
	emit_signal("change_easing_y", Easing.QUAD_EASE_IN)

func _on_CheckBox17_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox17.pressed = true
	emit_signal("change_easing_x", Easing.LINEAR_EASE_OUT)


func _on_CheckBox18_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox18.pressed = true
	emit_signal("change_easing_y", Easing.LINEAR_EASE_OUT)


func _on_CheckBox33_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox33.pressed = true
	emit_signal("change_easing_x", Easing.LINEAR_EASE_INOUT)


func _on_CheckBox34_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox34.pressed = true
	emit_signal("change_easing_y", Easing.LINEAR_EASE_INOUT)


func _on_CheckBox19_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox19.pressed = true
	emit_signal("change_easing_x", Easing.QUAD_EASE_OUT)


func _on_CheckBox20_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox20.pressed = true
	emit_signal("change_easing_y", Easing.QUAD_EASE_OUT)


func _on_CheckBox35_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox35.pressed = true
	emit_signal("change_easing_x", Easing.QUAD_EASE_INOUT)


func _on_CheckBox36_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox36.pressed = true
	emit_signal("change_easing_y", Easing.QUAD_EASE_INOUT)


func _on_CheckBox05_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox05.pressed = true
	emit_signal("change_easing_x", Easing.SIN_EASE_IN)


func _on_CheckBox21_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox21.pressed = true
	emit_signal("change_easing_x", Easing.SIN_EASE_OUT)


func _on_CheckBox37_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox37.pressed = true
	emit_signal("change_easing_x", Easing.SIN_EASE_INOUT)


func _on_CheckBox06_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox06.pressed = true
	emit_signal("change_easing_y", Easing.SIN_EASE_IN)


func _on_CheckBox22_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox22.pressed = true
	emit_signal("change_easing_y", Easing.SIN_EASE_OUT)


func _on_CheckBox38_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox38.pressed = true
	emit_signal("change_easing_y", Easing.SIN_EASE_INOUT)


func _on_CheckBox07_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox07.pressed = true
	emit_signal("change_easing_x", Easing.EXPO_EASE_IN)


func _on_CheckBox23_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox23.pressed = true
	emit_signal("change_easing_x", Easing.EXPO_EASE_OUT)


func _on_CheckBox39_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox39.pressed = true
	emit_signal("change_easing_x", Easing.EXPO_EASE_INOUT)


func _on_CheckBox08_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox08.pressed = true
	emit_signal("change_easing_y", Easing.EXPO_EASE_IN)


func _on_CheckBox24_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox24.pressed = true
	emit_signal("change_easing_y", Easing.EXPO_EASE_OUT)


func _on_CheckBox40_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox40.pressed = true
	emit_signal("change_easing_y", Easing.EXPO_EASE_INOUT)


func _on_CheckBox09_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox09.pressed = true
	emit_signal("change_easing_x", Easing.CIRC_EASE_IN)


func _on_CheckBox25_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox25.pressed = true
	emit_signal("change_easing_x", Easing.CIRC_EASE_OUT)


func _on_CheckBox41_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox41.pressed = true
	emit_signal("change_easing_x", Easing.CIRC_EASE_INOUT)


func _on_CheckBox10_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox10.pressed = true
	emit_signal("change_easing_y", Easing.CIRC_EASE_IN)


func _on_CheckBox26_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox26.pressed = true
	emit_signal("change_easing_y", Easing.CIRC_EASE_OUT)


func _on_CheckBox42_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox42.pressed = true
	emit_signal("change_easing_y", Easing.CIRC_EASE_INOUT)


func _on_CheckBox11_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox11.pressed = true
	emit_signal("change_easing_x", Easing.CUBE_EASE_IN)


func _on_CheckBox27_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox27.pressed = true
	emit_signal("change_easing_x", Easing.CUBE_EASE_OUT)


func _on_CheckBox43_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox43.pressed = true
	emit_signal("change_easing_x", Easing.CUBE_EASE_INOUT)


func _on_CheckBox12_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox12.pressed = true
	emit_signal("change_easing_y", Easing.CUBE_EASE_IN)


func _on_CheckBox28_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox28.pressed = true
	emit_signal("change_easing_y", Easing.CUBE_EASE_OUT)


func _on_CheckBox44_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox44.pressed = true
	emit_signal("change_easing_y", Easing.CUBE_EASE_INOUT)


func _on_CheckBox13_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox13.pressed = true
	emit_signal("change_easing_x", Easing.QUART_EASE_IN)


func _on_CheckBox29_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox29.pressed = true
	emit_signal("change_easing_x", Easing.QUART_EASE_OUT)


func _on_CheckBox45_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox45.pressed = true
	emit_signal("change_easing_x", Easing.QUART_EASE_INOUT)


func _on_CheckBox14_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox14.pressed = true
	emit_signal("change_easing_y", Easing.QUART_EASE_IN)


func _on_CheckBox30_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox30.pressed = true
	emit_signal("change_easing_y", Easing.QUART_EASE_OUT)


func _on_CheckBox46_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox46.pressed = true
	emit_signal("change_easing_y", Easing.QUART_EASE_INOUT)


func _on_CheckBox15_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox15.pressed = true
	emit_signal("change_easing_x", Easing.QUINT_EASE_IN)


func _on_CheckBox31_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox31.pressed = true
	emit_signal("change_easing_x", Easing.QUINT_EASE_OUT)


func _on_CheckBox47_pressed() -> void:
	_clear_x()
	$Panel/GridContainer/CheckBox47.pressed = true
	emit_signal("change_easing_x", Easing.QUINT_EASE_INOUT)


func _on_CheckBox16_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox16.pressed = true
	emit_signal("change_easing_y", Easing.QUINT_EASE_IN)


func _on_CheckBox32_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox32.pressed = true
	emit_signal("change_easing_y", Easing.QUINT_EASE_OUT)


func _on_CheckBox48_pressed() -> void:
	_clear_y()
	$Panel/GridContainer/CheckBox48.pressed = true
	emit_signal("change_easing_y", Easing.QUINT_EASE_INOUT)


func _on_CheckBox2_01_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_01.pressed = true
	emit_signal("change_easing_x", Easing.ELASTIC_EASE_IN)


func _on_CheckBox2_07_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_07.pressed = true
	emit_signal("change_easing_x", Easing.ELASTIC_EASE_OUT)


func _on_CheckBox2_13_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_13.pressed = true
	emit_signal("change_easing_x", Easing.ELASTIC_EASE_INOUT)


func _on_CheckBox2_02_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_02.pressed = true
	emit_signal("change_easing_y", Easing.ELASTIC_EASE_IN)


func _on_CheckBox2_08_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_08.pressed = true
	emit_signal("change_easing_y", Easing.ELASTIC_EASE_OUT)


func _on_CheckBox2_14_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_14.pressed = true
	emit_signal("change_easing_y", Easing.ELASTIC_EASE_INOUT)


func _on_CheckBox2_03_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_03.pressed = true
	emit_signal("change_easing_x", Easing.BOUNCE_EASE_IN)


func _on_CheckBox2_09_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_09.pressed = true
	emit_signal("change_easing_x", Easing.BOUNCE_EASE_OUT)


func _on_CheckBox2_15_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_15.pressed = true
	emit_signal("change_easing_x", Easing.BOUNCE_EASE_INOUT)


func _on_CheckBox2_04_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_04.pressed = true
	emit_signal("change_easing_y", Easing.BOUNCE_EASE_IN)


func _on_CheckBox2_10_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_10.pressed = true
	emit_signal("change_easing_y", Easing.BOUNCE_EASE_OUT)


func _on_CheckBox2_16_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_16.pressed = true
	emit_signal("change_easing_y", Easing.BOUNCE_EASE_INOUT)


func _on_CheckBox2_05_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_05.pressed = true
	emit_signal("change_easing_x", Easing.BACK_EASE_IN)


func _on_CheckBox2_11_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_11.pressed = true
	emit_signal("change_easing_x", Easing.BACK_EASE_OUT)


func _on_CheckBox2_17_pressed() -> void:
	_clear_x()
	$Panel/GridContainer2/CheckBox2_17.pressed = true
	emit_signal("change_easing_x", Easing.BACK_EASE_INOUT)


func _on_CheckBox2_06_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_06.pressed = true
	emit_signal("change_easing_y", Easing.BACK_EASE_IN)


func _on_CheckBox2_12_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_12.pressed = true
	emit_signal("change_easing_y", Easing.BACK_EASE_OUT)


func _on_CheckBox2_18_pressed() -> void:
	_clear_y()
	$Panel/GridContainer2/CheckBox2_18.pressed = true
	emit_signal("change_easing_y", Easing.BACK_EASE_INOUT)
