extends Node2D

onready var Easing = preload("res://godot_easing/easing.gd")

func _ready() -> void:

	testEasing()


func testEasing():

	var startValue = 0.0
	var endValue = 1.0
	var change = 1.0
	var duration = 1.0

	print(Easing.Cubic.easeOut(0, startValue, change, duration))						# --> 0
	print(Easing.Cubic.easeOut(duration / 4.0, startValue, change, duration))			# --> 0.578125
	print(Easing.Cubic.easeOut(duration / 2.0, startValue, change, duration))			# --> 0.875
	print(Easing.Cubic.easeOut(duration / (3.0/4.0), startValue, change, duration))		# --> 1.037037
	print(Easing.Cubic.easeOut(duration, startValue, change, duration))					# --> 1
