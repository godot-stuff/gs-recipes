# SpockerDotNet - Godot Style Guide


**This guide is a compliment to the [official GDScript style guide](http://docs.godotengine.org/en/latest/getting_started/scripting/gdscript/gdscript_styleguide.html) It is a guide for all Godot project produced by my company.

___

This is an unofficial style guide for [Godot](https://godotengine.org), a free, libre and open source game engine.

## Conventions used in this document
- `snake_case` is **like_this**.
- `camelCase` is **likeThis**.
- `PascalCase` is **LikeThis**.
- `UPPER_SNAKE_CASE` is **LIKE_THIS**.

___

## Naming
- Use `UPPER_SNAKE_CASE` for constants.
- Use `snake_case` for variables, functions and signals.
- Use `snake_case` for input map names.
- Use `snake_case` for all file names.
- Use `PascalCase` for node, class and object names.

## Functions
- Function arguments should have a space after each comma, like this:

```gdscript
func my_function(a, b, c):
    pass
```
## Signals
- See the ```"signal_work"``` Scene for an example using signals.
- All Signals should start with the word ```signal```
- Signals should contain the Object it is associated with, for example:
```
signal_game* - all signals related to the game
signal_player* - all signals related to the player
```
- Encapsulated in a Global script with a Constant for each Signal in the game, like this:
```
Extends Node

const SIGNAL_PLAYER_WAS_HIT = "signal_player_was_hit"
signal signal_player_was_hit()

```
- Give Signals the concept of Before and After like this:
```
signal_game_is_ending
signal_game_has_ended
```

## References
- See the ```game_work``` Scene for an example of using References.
- Encapsulate all References in a script that can be Autoload(ed) in the Project, like this:
```
Extends Node

const MyClass = preload("res://my_game/scripts/classes/my_class.gd")
```
- This lets you create an reference in your other scripts like this:
```
var MyClass = game.MyClass
```
- Which you can then use to create new Objects like this:
```
var the_class = MyClass.new()
```

## Comments

- Begin comments with an uppercase letter, unless you are referencing a function or a variable. Do not end them with a period, unless the comment has several sentences.

- Comments should have a space after the `#` symbol, and should be indented as usual.

Example:

```gdscript
# Outputs "Hello world!" to console
print("Hello world!")
```

## Indentation

- **Always** use tabs for indentation, GDScript does not like spaces anyway.

## Line lengths

- Try to keep lines under 80 characters. Disregarding this guideline at times is acceptable, but over 100 characters is definitely too much.

## Spacing

- Put spaces around operators. Example:

```gdscript
print(str(5 * 40 + 2))
```

## File types and extensions

- In most cases, you should use `.tscn` scenes and `.tres` resources as those are more friendly towards version control systems.

## Directory structure

Use this Project as an example of how your directory structure should be organized. However, in general, a project will contain three main folders, based on the name of the project. For example,

```
addons
my_game
my_game_tests
my_game_work
```


## `.gitignore`

If using Git, you should use a `.gitignore` file that ignores certain patterns from being added to your Git repository. It should be placed at the root of the game folder and should ignore everyhinh except for your game folders. Here's an example that will suit most Godot projects:

```
*
!my_game
```

# License

Copyright (c) 2018 SpockerDotNet LLc

CC0 1.0 Universal, see [LICENSE.md](https://creativecommons.org/publicdomain/zero/1.0/legalcode).