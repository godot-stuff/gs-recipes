
"""

This Scene gives an example of how
to use the Signals Autoload in 
you Game.

Signals can be connected at any
time in the Game.

"""

extends Node

func _start_game():
	print("** Godot Has Started **")
	

func _init():
	signals.connect(signals.SIGNAL_START_THE_GAME, self, "_start_game")
	
	
func _ready():
	signals.emit_signal(signals.SIGNAL_START_THE_GAME)