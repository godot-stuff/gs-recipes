
"""
	Signals should be kept in a 
	seperate script and added in
	the Autoload.
	
	This will make them accessible
	from any script in the game and
	provides a central location
	for connecting to.
	
"""
extends Node

#	game signals

const SIGNAL_GAME_IS_ENDING = "signal_game_is_ending"
const SIGNAL_GAME_HAS_ENDED = "signal_game_has_ended"
const SIGNAL_GAME_IS_STARTING = "signal_game_is_starting"
const SIGNAL_GAME_HAS_STARTED = "signal_game_has_started"
const SIGNAL_START_THE_GAME = "signal_start_the_game"
const SIGNAL_END_THE_GAME = "signal_end_the_game"

signal signal_game_is_ending()
signal signal_game_has_ended()
signal signal_game_is_starting()
signal signal_game_has_started()
signal signal_start_the_game()
signal signal_end_the_game()

#	score signals

const SIGNAL_SCORE_IS_CHANGING = "signal_score_is_changing"
const SIGNAL_SCORE_HAS_CHANGED = "signal_score_has_changed"

signal signal_score_is_changing(score)
signal signal_score_has_changed(score)