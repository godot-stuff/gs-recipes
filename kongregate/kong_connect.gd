extends Node

onready var user_name_label = find_node("_username")
onready var user_id_label = find_node("_userid")
onready var message_label = find_node("_message")

onready var Kongregate = preload("gs_kongregate.gd")

var kongregate
var user_name
var user_id
var auth_token
var api_key = "f34b3270-5866-4a37-ac0a-fd08f8724e0d"

	

func _ready():
	
	#	create kong object
	kongregate = Kongregate.new()
	
	#	get the username and id
	user_name = kongregate.get_username()
	user_id = kongregate.get_userid()
	auth_token = kongregate.get_game_auth_token()
	user_name_label.text = "%s" % [user_name]
	user_id_label.text = "%s" % [user_id]
	print(user_name)
	print(user_id)
	
	kongregate.connect("kongregate_authenticate", self, "_on_authenticate")
	
	
func _on_authenticate(response):
	message_label.text = response.username
	
func _on_login():
	message_label.text = "login event for: %s" %  [kongregate.get_username()]


func _on_Services_GetGameAuthToken_pressed():
	var auth_token = kongregate.get_game_auth_token()
	message_label.text = "%s" % [auth_token]
	print(auth_token)


func _on_Stats_Submit_pressed():
	kongregate.submit_stats("test", 12345)


func _on_Services_IsGuest_pressed():
	var is_guest = kongregate.get_is_guest()
	message_label.text = "%s" % [is_guest]


func _on_Servces_ShowInvitationBox_pressed():
	kongregate.show_invitation_box("Please Tell Your Friends.")


func _on_Services_PrivateMessage_pressed():
	kongregate.send_private_message("Well, Hello There!")


func _on_Authenticate_pressed():
#	kongregate.authenticate(user_id, auth_token, api_key)
#	var headers = ["Access-Control-Allow-Origin : *"]
	var headers = ["Content-Type: application/json"]
	$HTTPRequest.request("https://api.kongregate.com/api/authenticate.json?user_id=4594680&game_auth_token=d81c97ef4e939f79e8bd4d36e643c1ee4c98a756d0e1b4e849b132edfbd0cb30&api_key=f34b3270-5866-4a37-ac0a-fd08f8724e0d", [], true)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	print(result)
	print(response_code)
	print(body.get_string_from_ascii())
