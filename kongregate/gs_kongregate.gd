
"""
Class: Kongregate

	A Implementation of the Kongregate API for Godot
	
Remarks:
	
	
API:
f34b3270-5866-4a37-ac0a-fd08f8724e0d
Token:
User:
	
	None
"""

extends Node

signal kongregate_authenticate(response)

var http
var waiting
var ticks

enum http_state { CONNECTING, REQUESTING, RESPONDING, IDLE }


func _init():
	set_process(true)

func _process(delta):
	
	print("hello again")
	print(http_state)
	
	if http_state == IDLE:
		return
		
	ticks += 1
	
	if ticks < 60:
		return
		
	ticks = 0
	
	http.poll()
	print(http.get_status())
	
	if http_state == CONNECTING:
		if http.get_status() == HTTPClient.STATUS_CONNECTED:
			http_state = REQUESTING
			print("connected")
			return
		else:
			print("connecting...")
	
	if http_state == REQUESTING:
		if http.get_status() == HTTPClient.STATUS_BODY:
			http_state = RESPONDING
			http.request(http.METHOD_GET, "/api/authenticate.json", [])
			print("request completed")
			return
		else:
			print("requesting...")
		
	if http_state == RESPONDING:
		if http.get_status() == HTTPClient.STATUS_BODY:
			print("getting body")
		else:
			http_state = IDLE
			print("all done")
	

func get_username():
	return JavaScript.eval("kongregate.services.getUsername()")
	

func get_userid():
	return JavaScript.eval("kongregate.services.getUserId()")
	

func get_game_auth_token():
	return JavaScript.eval("kongregate.services.getGameAuthToken()")
	
	
func get_is_guest():
	return JavaScript.eval("kongregate.services.isGuest()")
	

func send_private_message(message):
	JavaScript.eval("kongregate.services.privateMessage(\"%s\")" % [message])
	
	
func show_invitation_box(invitation, filter="", kv_parms={}):
	JavaScript.eval("kongregate.services.showInvitationBox(\"%s\")" % [invitation])


func submit_stats(stat_name, stat_value):
	var s = "kongregate.stats.submit(\"%s\", %s)" % [stat_name, stat_value]
	print(s)
	JavaScript.eval(s)
	
func authenticate(user_id, auth_token, api_key):
	_submit_authenticate(user_id, auth_token, api_key)	
#	var response = AuthResponse.new()
#	response.success = true
#	response.username = "TestUser"
#	response.user_id = 12345
#	emit_signal("kongregate_authenticate", response)
	
func _submit_authenticate(user_id, auth_token, api_key):
	print("hello world")
	http = HTTPClient.new()
	
	var err = http.connect_to_host("api.kongregate.com", 443, true)
	
	http_state = CONNECTING
	
	return
	
	
	print(err)
	assert(err == OK)
	while http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING:
		http.poll()
		print("connecting...")
		OS.delay_msec(500)
	assert(http.get_status() == HTTPClient.STATUS_CONNECTED) # Could not connect	
	var req = { "api_key": api_key, "user_id": user_id, "game_auth_token": auth_token }
#	var qs = http.query_string_from_dict(req)
#	var headers = ["User-Agent: Pirulo/1.0 (Godot)", "Accept: */*"]
	var headers = []
#	print(qs)
	err = http.request(http.METHOD_GET, "/api/authenticate.json", headers)
	while http.get_status() == HTTPClient.STATUS_REQUESTING:
		http.poll()
		print("requesting...")
		OS.delay_msec(500)
	assert(http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED) # Make sure request finished well
	if http.has_response():
		if http.is_response_chunked():
			print("chunked")
		else:
			print("not chunked")
			
	var rb = PoolByteArray()
	while http.get_status() == HTTPClient.STATUS_BODY:
		http.poll()
		var chunk = http.read_response_body_chunk()
		if chunk.size() == 0:
			OS.delay_msec(500)
		else:
			rb += chunk
			
	print ("bytes:", rb.size())
	print("response:", rb.get_string_from_ascii())
			
	waiting = true
			
	
class AuthResponse:
	var success
	var username
	var user_id