extends System
class_name MovementSystem

func on_process_entity(entity : Entity, delta  : float) -> void:
	
	var _movement = entity.get_component('movementcomponent') as Movement
	
	var _speed = (_movement.speed + _movement.modifier)
	print(_speed)
	
	entity.position += _movement.direction * _speed * _movement.speed_factor * delta
