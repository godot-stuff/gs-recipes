extends Entity

func on_ready():
	
	var _components = get_components()
	for _component in _components:
		if (_component is TileComponent):
			$Label.text = _component.tile_name
