class_name Movement
extends Component

export var speed : float = 10.0
export var speed_factor : float = 10.0

var direction : Vector2 = Vector2.RIGHT
var modifier : float = 0.0
