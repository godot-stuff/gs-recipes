extends System
class_name SpeedModifierSystem


func on_process_entity(entity : Entity, delta : float):
	
	var _collisions = entity.get_overlapping_areas()
	var _movement = entity.get_component('movementcomponent') as Movement
	var _modifier = 0.0
	
	for _area in _collisions:
		var _tile = _area.get_component('tilecomponent') as TileComponent
		if (_tile.speed_modifier > _modifier):
			_modifier = _tile.speed_modifier
		_modifier = _tile.speed_modifier
		
	_movement.modifier = _modifier
