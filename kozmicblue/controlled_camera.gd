extends Camera2D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var _dir = Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right")
#	print(_dir)
	rotation_degrees += (_dir * 150)  * delta
