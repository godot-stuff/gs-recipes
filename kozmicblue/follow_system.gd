extends Node2D

export(NodePath) var camera
export(NodePath) var player

func _process(delta):
	
	# rotate camera
	var _camera : Camera2D = get_node(camera)
#	print(_camera)

	var _player = get_node(player) as Node2D
	
	_camera.position = _player.position
