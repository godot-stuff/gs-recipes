class_name KinematicBlue
extends KinematicBody2D

var max_speed
var speed = 200
var dir = Vector2.DOWN
var bounce_coefficient = 1.0

var _on_wall

func _physics_process(delta):
	

	var vel = dir * speed * delta
	print(vel.length())

#	move_and_slide(vel, Vector2.UP, false, 1, 1, false)
	move_and_slide(vel)
#	print("c: {0} f: {1} w: {2} #: {3}".format([is_on_ceiling(), is_on_floor(), is_on_wall(), get_slide_count()]))


	if get_slide_count() > 0:
		
		var collision = get_slide_collision(0)
		dir = dir.bounce(collision.normal)
		
#		print(dir.length())
		
		_on_wall = false
		if (dir.length() < 10):
			_on_wall = true
			
		if (_on_wall):
			return
				
#		vel = collision.reflect(vel) * bounce_coefficient
		dir += dir * -0.25
		
