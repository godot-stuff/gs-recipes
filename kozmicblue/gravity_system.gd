extends Node2D

export(NodePath) var camera
export(NodePath) var kinematic_blue

func _process(delta):
	
	# rotate camera
	var _camera : Camera2D = get_node(camera)
#	print(_camera)

#	var _kblue : KinematicBlue = get_node(kinematic_blue)
#	print(_kblue)
	
#	_camera.rotation_degrees += delta * 60
#
#	# rotate gravity
	var _rot = _camera.rotation_degrees + 90
	_rot = deg2rad(_rot)
	var _dir = Vector2(cos(_rot), sin(_rot)) * 4
##	print(_dir)
	Physics2DServer.area_set_param(get_world_2d().space, Physics2DServer.AREA_PARAM_GRAVITY_VECTOR, _dir)
	
	# rotate blue
#	_kblue.dir += _dir.normalized()
	
	pass
