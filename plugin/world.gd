extends Node2D
var t = 0.0
func _process(delta):
	t += delta * 5
	get_node("icon").position = Vector2(0, cos(t) * 100)
