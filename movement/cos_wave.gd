"""
https://gamemechanicexplorer.com/#platformer-5
https://www.gamedev.net/forums/topic/603697-sine-wave-sprite-movement/

"""
extends Node

export (float) var FREQUENCY
export (float) var MAGNITUDE
var freq

var parent
var numberOfTicks = 0.0

func _process(delta):
	
	numberOfTicks += 1 / FREQUENCY
	parent.position.y = (cos(numberOfTicks * PI) * MAGNITUDE) + (parent.global_position.y)


func _ready():
	parent = get_parent()
	freq = 1 / FREQUENCY