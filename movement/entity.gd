extends Node2D

export (int) var SPEED
export (int) var SPEED_FACTOR

func _process(delta):
	
	position += Vector2(1,0) * SPEED * SPEED_FACTOR * delta

